from dataclasses import dataclass

from music21.key import Key
from music21.note import Note
from music21.stream import Part


@dataclass
class Composition:
    part: Part
    key_name_by_last_note: str | None
    possible_keys: (Key, Key)

    def resolve_tonic(self) -> Note:
        if self.key_name_by_last_note is not None:
            return Note(self.key_name_by_last_note)

        for key in self.part.analyze('key').alternateInterpretations:
            if key in self.possible_keys:
                return Note(key.tonic.name)
