from collections import deque

from music21.chord import Chord
from music21.key import Key
from music21.stream.base import Score, Part, Opus, Measure

from src.composition import Composition


def get_colors_by_offset(offset: int) -> [str]:
    colors = [
        'red',
        'orange',
        'yellow',
        'green',
        'blue',
        'indigo',
        'violet',
        'red',
        'orange',
        'yellow',
        'green',
        'blue'
    ]
    colors_deque = deque(colors)
    colors_deque.rotate(offset)
    return list(colors_deque)


def get_compositions(part: Part) -> [Composition]:
    def get_tonic_by_last_note(last_note: str) -> str | None:
        if last_note in (possible_key.tonic.name, possible_key.parallel.tonic.name):
            return last_note
        return None

    def save_composition():
        key = None
        notes = list(last_measure.notes)
        if notes:
            last = notes[-1]
            if last.isChord:
                key = get_tonic_by_last_note(Chord(last.pitches).bass().name)
            if last.isNote:
                key = get_tonic_by_last_note(last.pitches[0].name)
        compositions.append(
            Composition(current_composition_part, key, (possible_key, possible_key.parallel)))

    compositions: [Composition] = []
    current_composition_part: Part | None = None
    possible_key: Key | None = None
    last_measure: Measure | None = None
    for measure in part.getElementsByClass('Measure'):
        if measure.keySignature is None:
            current_composition_part.append(measure)
            last_measure = measure
            continue
        if current_composition_part is not None:
            save_composition()
        possible_key = measure.keySignature.asKey()
        last_measure = measure
        current_composition_part = Part()
        current_composition_part.append(measure)
    if current_composition_part is not None:
        save_composition()
    return compositions


def paint_notes(stream_object: Score | Part | Opus) -> None:
    if type(stream_object) is Opus:
        for score in stream_object.scores:
            paint_notes(score)
        return

    if type(stream_object) is Score:
        for part in stream_object.parts:
            paint_notes(part)
        return

    compositions = get_compositions(stream_object)
    for composition in compositions:
        tonic_note = composition.resolve_tonic()
        colors = get_colors_by_offset(tonic_note.pitch.midi % 12)
        for not_rest in composition.part.recurse().notes:
            if not_rest.isNote:
                color_index = not_rest.pitch.midi % 12
                not_rest.style.color = colors[color_index]
            elif not_rest.isChord:
                for chord_note in not_rest.notes:
                    color_index = chord_note.pitch.midi % 12
                    chord_note.style.color = colors[color_index]
