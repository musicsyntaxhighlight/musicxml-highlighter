import os
import uuid
import base64
from xml.etree.ElementTree import ParseError

from music21 import converter
from music21.converter import ConverterFileException
import tempfile

from pyparsing import ParseException

from src import painter


def handler(event, context):
    musxml = event['body']
    musxml = base64.b64decode(musxml).decode('utf-8')

    print(event)
    base_name = str(uuid.uuid4())
    with open(f'/tmp/{base_name}.musicxml', 'w') as f:
        f.write(musxml)

    try:
        stream = converter.parseFile(f'/tmp/{base_name}.musicxml')
    except (ConverterFileException, ParseError) as e:
        return {
            'statusCode': 400,
            'body': e
        }
    stream = converter.parseFile(f'/tmp/{base_name}.musicxml')
    painter.paint_notes(stream)
    name = str(uuid.uuid4())
    stream.write(fp='/tmp/' + name, fmt='musicxml')
    with open('/tmp/' + name + '.musicxml', 'r') as f:
        painted = f.read()
    return {
        'statusCode': 200,
        'body': painted
    }
